package com.example.playsoundexample;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Product>listMusic;
    private ListView list;
    ProductListViewAdapter productListViewAdapter;

    private Button btn_playmusic;
    private ImageButton btn_stop;
    private TextView txtUrl;
    private Button btn_speaker;
    private Button btn_earphone;
    private ImageButton btn_next;
    private ImageButton btn_previous;
    private MediaPlayer mPlayer;
    private File file;
    private ProgressDialog prgDialog;
    public static final int progress_bar_type = 0;
    private Context  context;
    int position = 0;
    int positioncancel =-1;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final AudioManager mAudioMgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        listMusic = new ArrayList<>();

        listMusic.add(new Product("http://143.110.188.42:8080/datasite/soundeffect/am_thah_cua_sa_mac.mp3", "Âm thanh của xa mạc", 1));
        listMusic.add(new Product("http://143.110.188.42:8080/datasite/soundeffect/bai_tieu_buon.mp3", "Bài tiêu buồn", 2));
        listMusic.add(new Product(  "http://143.110.188.42:8080/datasite/soundeffect/beautiful_piano.mp3", "piano", 3));


        list = findViewById(R.id.list);
        productListViewAdapter = new ProductListViewAdapter(listMusic);
        list.setAdapter(productListViewAdapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
                public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final Product product = listMusic.get(position);
                txtUrl.setText(listMusic.get(position).getName());
//                txtUrl.setText(file.getPath());

                if (product.isDownloaded()){
                    MainActivity.this.playMusic(listMusic.get(position).linkMusic);
                    productListViewAdapter.setPositionSelectedItem(position);
                }else {
                    new DownloadMusicInternet(product, new OnDownLoadedListener() {
                        @Override
                        public void downloaded() {
                            productListViewAdapter.setPositionSelectedItem(position);
                        }
                    }).execute(product.linkMusic);
                }
        }
        });


        txtUrl = (TextView) findViewById(R.id.textMusic);
        btn_stop = (ImageButton) findViewById(R.id.btn_stop);
        btn_speaker = (Button) findViewById(R.id.btn_speaker);
        btn_earphone = (Button) findViewById(R.id.btn_earphone);
        btn_playmusic = (Button) findViewById(R.id.btn_playMusic);
        btn_next = (ImageButton) findViewById(R.id.btn_next);
        btn_previous = (ImageButton) findViewById(R.id.btn_previous);

        mPlayer = new MediaPlayer();
        file = new File(getDir("filesdir", Context.MODE_PRIVATE) + "/nhac.mp3");


        btn_playmusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listMusic.size() == 0) return;
                playMusic(listMusic.get(position).getLinkMusic());
                productListViewAdapter.setPositionSelectedItem(position);
                txtUrl.setText(listMusic.get(position).getName());

            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listMusic.size() <= 0) return;
                if (listMusic.size() == 1) {
                    playMusic(listMusic.get(position).getLinkMusic());
                } else {
                    if (position >= listMusic.size() - 1) {
                        position = 0;
                    } else {
                        position++;
                    }
                    playMusic(listMusic.get(position).getLinkMusic());
                    productListViewAdapter.setPositionSelectedItem(position);
                    txtUrl.setText(listMusic.get(position).getName());
                }
            }
        });

        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listMusic.size() <= 0) return;
                if (listMusic.size() == 1) {
                    playMusic(listMusic.get(position).getLinkMusic());
                } else {
                    if (position < listMusic.size() - 1) {
                        position = 0;
                        Toast.makeText(getApplicationContext(), "Đã hết bài hát ", Toast.LENGTH_LONG).show();
                    } else {
                        position--;
                    }
                    playMusic(listMusic.get(position).getLinkMusic());
                    productListViewAdapter.setPositionSelectedItem(position);
                    txtUrl.setText(listMusic.get(position).getName());
                }
            }
        });

        btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayer.stop();
                txtUrl.setText("");
                productListViewAdapter.setPositionSelectedItem(positioncancel);
            }
        });



        btn_earphone.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                if (mAudioMgr.isWiredHeadsetOn()) {
                    mAudioMgr.setWiredHeadsetOn(false);
                    mAudioMgr.setSpeakerphoneOn(true);
                    mAudioMgr.setMode(AudioManager.MODE_IN_COMMUNICATION);
                } else {
                    mAudioMgr.setMode(AudioManager.MODE_IN_COMMUNICATION);
                    mAudioMgr.setSpeakerphoneOn(false);
                    mAudioMgr.setWiredHeadsetOn(true);
                }
            }
        });

        btn_speaker.setOnClickListener(new View.OnClickListener() {
            @SuppressWarnings("deprecation")
            @Override
            public void onClick(View v) {
                if (mAudioMgr.isWiredHeadsetOn()) {
                    mAudioMgr.setWiredHeadsetOn(true);
                    mAudioMgr.setSpeakerphoneOn(false);
                    mAudioMgr.setMode(AudioManager.MODE_IN_COMMUNICATION);
                } else {
                    mAudioMgr.setMode(AudioManager.MODE_IN_COMMUNICATION);
                    mAudioMgr.setSpeakerphoneOn(true);
                    mAudioMgr.setWiredHeadsetOn(false);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        if (mPlayer != null) {
            if (mPlayer.isPlaying()) {
                mPlayer.pause();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        if (mPlayer != null) {
            if (mPlayer.isPlaying()) {
                mPlayer.stop();
            }
            mPlayer.release();
            mPlayer = null;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if ((mPlayer != null) && (!mPlayer.isPlaying())) {
            mPlayer.start();
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                prgDialog = new ProgressDialog(this);
                prgDialog.setMessage("Downloading Mp3 file. Please wait...");
                prgDialog.setIndeterminate(false);
                prgDialog.setMax(100);
                prgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                prgDialog.setCancelable(false);
                prgDialog.show();
                return prgDialog;
            default:
                return null;

        }
    }

    class DownloadMusicInternet extends AsyncTask<String, String, String> {
        String fileName = "";
        Product product;
        OnDownLoadedListener downLoadedListener;

        public DownloadMusicInternet(Product product, OnDownLoadedListener downLoadedListener) {
            this.product = product;
            this.downLoadedListener = downLoadedListener;
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                file = new File(getFilesDir() + "/" + product.name + ".mp3");
                boolean isCreateFileSuccess;
//                file.delete();
                isCreateFileSuccess = file.createNewFile();
                if (isCreateFileSuccess) {
                    URL url = new URL(f_url[0]);
                    URLConnection conection = url.openConnection();
                    conection.connect();
                    int lenghtOfFile = conection.getContentLength();
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);
                    OutputStream output = new FileOutputStream(file);
                    byte data[] = new byte[2048];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        output.write(data, 0, count);
                        publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    }
                    output.flush();
                    output.close();
                    input.close();

                }

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }
            return file.getPath();
        }

        protected void onProgressUpdate(String... progress) {
            // Set progress percentage
            prgDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @SuppressWarnings("deprecation")
        @Override
        protected void onPostExecute(String s) {
            product.setDownloaded(true);
            System.out.println("Downloaded");
            dismissDialog(progress_bar_type);
            playMusic(s);
            downLoadedListener.downloaded();
        }
    }

    protected void playMusic(String path) {
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
        }
        if (path == null || path.isEmpty()) return;
        mPlayer = new MediaPlayer();

        try {
            mPlayer.setDataSource(path);
            mPlayer.prepare();
            mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {
                    // TODO Auto-generated method stub

                }
            });
        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), "You might not set the URI correctly!", Toast.LENGTH_LONG).show();
        } catch (SecurityException e) {
            Toast.makeText(getApplicationContext(), "URI cannot be accessed, permissed needed", Toast.LENGTH_LONG).show();
        } catch (IllegalStateException e) {
            Toast.makeText(getApplicationContext(), "IllegalStateException", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), "IO Error occured", Toast.LENGTH_LONG).show();
        }
    }

    class Product {
        String name;
        String linkMusic;
        int IDMusic;
        boolean isDownloaded;

        public Product(String linkMusic, String name, int IDMusic) {
            this.name = name;
            this.linkMusic = linkMusic;
            this.IDMusic = IDMusic;
        }

        public String getName() {
            return name;
        }

        public String getLinkMusic() {
            return linkMusic;
        }

        public boolean isDownloaded() {
            return isDownloaded;
        }

        public void setDownloaded(boolean downloaded) {
            isDownloaded = downloaded;
        }
}

    class ProductListViewAdapter extends BaseAdapter {

        private final ArrayList<Product> listProduct;

        private int positionSelected = -1;

        ProductListViewAdapter(ArrayList<Product> listProduct) {
            this.listProduct = listProduct;
        }

        @Override
        public int getCount() {

            return listProduct.size();
        }

        @Override
        public Object getItem(int position) {

            return listProduct.get(position);
        }

        @Override
        public long getItemId(int position) {

            return listProduct.get(position).IDMusic;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View viewProduct;
            if (convertView == null) {
                viewProduct = View.inflate(parent.getContext(), R.layout.product_view, null);
            } else viewProduct = convertView;

                final ImageButton btnplay = (ImageButton) viewProduct.findViewById(R.id.btn_play);
                 final ImageButton btnpause = (ImageButton) viewProduct.findViewById(R.id.btn_pause);

                 btnplay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        btnpause.setVisibility(View.VISIBLE);
                        btnplay.setVisibility(View.GONE);
                        mPlayer.start();
                    }
                });

                 btnpause.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         btnpause.setVisibility(View.INVISIBLE);
                         btnplay.setVisibility(View.VISIBLE);
                         mPlayer.pause();
                     }
                 });

                if(positionSelected == position ){
                    btnplay.setVisibility(View.GONE);
                    btnpause.setVisibility(View.VISIBLE);
                    viewProduct.setBackgroundColor(Color.GRAY);

                }else{
                    btnplay.setVisibility(View.GONE);
                    btnpause.setVisibility(View.GONE);
                    viewProduct.setBackgroundColor(Color.TRANSPARENT);
                }

            Product product = (Product) getItem(position);
            ((TextView) viewProduct.findViewById(R.id.idmusic)).setText(String.format("ID = %d", product.IDMusic));
            ((TextView) viewProduct.findViewById(R.id.namemusic)).setText(String.format("Tên bài hát : %s", product.name));
            ((TextView) viewProduct.findViewById(R.id.linkmusic)).setText(String.format("link : %s", product.linkMusic));
            return viewProduct;
        }

        public void setPositionSelectedItem(int positionSelectedItem) {
            this.positionSelected = positionSelectedItem;
            notifyDataSetChanged();
        }
    }
    interface OnDownLoadedListener {
        void downloaded();
    }


}
